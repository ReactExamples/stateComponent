var path = require('path');
var webpack = require('webpack');
var WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    entry: [
        "./app/main"
        ],
    resolve: {extensions: ['.js','.jsx']},
    output: {
        path: path.join(__dirname,'./public/dist/'),
        filename: 'main.bundle.js',
    },
    module:{
        loaders:[
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query:{presets: ['react','es2015']}
            }
        ]
    },
    devServer:{
        publicPath: '/',
        contentBase: './public',
        hot: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new WebpackNotifierPlugin()
    ]
}