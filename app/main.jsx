import React from 'react';
import ReactDOM from 'react-dom';
import Box from './components/box';

ReactDOM.render(
    <Box/>,
    document.getElementById('content')
);